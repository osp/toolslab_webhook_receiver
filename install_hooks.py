import gitlab

gl = gitlab.Gitlab.from_config()

group = gl.groups.get(8)

for project in group.projects:
    print(project)
    for hook in project.hooks.list():
        hook.delete()
    project.hooks.create({'url': 'http://webhook.osp.kitchen', 'push_events': 1})
