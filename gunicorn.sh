#! /usr/bin/env bash

NAME="webhook"                               # Name of the application
USER=www-data                             # the user to run as
GROUP=www-data                            # the group to run as
DJANGODIR=/home/osp/apps/osp.tool.gitlab_webhook_receiver  # Django project directory
SOCKFILE=/tmp/gunicorn-hook.sock               # we will communicte using this unix socket
NUM_WORKERS=3                             # how many worker processes should Gunicorn spawn
# DJANGO_SETTINGS_MODULE=le75.settings      # which settings file should Django use
# DJANGO_WSGI_MODULE=le75.wsgi              # WSGI module name
# 
VENV=/home/osp/apps/venvs/osp.tool.gitlab_webhook_receiver
# 
echo "Starting $NAME as `whoami`"
# 
# Activate the virtual environment
cd $DJANGODIR
source $VENV/bin/activate
# export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
# export PYTHONPATH=$DJANGODIR:$PYTHONPATH
# 
# Create the run directory if it doesn't exist
# RUNDIR=$(dirname $SOCKFILE)
# test -d $RUNDIR || mkdir -p $RUNDIR

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec $VENV/bin/gunicorn app:app \
  --name $NAME \
  --workers $NUM_WORKERS \
  --user=$USER --group=$GROUP \
  --bind=unix:$SOCKFILE \
  --log-level=debug \
  --log-file=-

