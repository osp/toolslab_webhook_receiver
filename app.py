import os
import shlex
import subprocess
import json

from bottle import request, route, run, default_app


REPO_PATH = "/home/osp/repos"


@route('/', method="POST")
def index():
    print("received request")
    if request.headers.get('X-Gitlab-Event') == 'Push Hook':
        print("push hook")
        data = json.load(request.body)
        #print(data)

        repo = data.get("repository")
        git_http_url = repo.get("git_http_url")
        project = data.get("project")
        print(project)
        branch = project.get("default_branch")
        #ns = data.get("namespace")
        ns = "osp"

        repo_name = git_http_url.split("/")[-1]
        repo_local_path = os.path.join(REPO_PATH, "{}.{}".format(ns, repo_name))

        if os.path.isdir(repo_local_path):
            print("repo exists. Pulling")
            cmd = "git fetch origin %s:%s" % (branch, branch)
            subprocess.Popen(shlex.split(cmd), cwd=repo_local_path)
        else:
            print("repo don't exist. Cloning")
            cmd = "git clone --bare {} {}.{}".format(git_http_url, ns, repo_name)
            subprocess.Popen(shlex.split(cmd), cwd=REPO_PATH)

        return "Hello World!"
    return "Bye World!"


if __name__ == "__main__":
    run(host='0.0.0.0', port=7331)
else:
    app = default_app()
